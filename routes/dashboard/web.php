<?php
/**
 * Created by PhpStorm.
 * User: elhou
 * Date: 08/10/2019
 * Time: 19:55
 */
Route::prefix('dashboard')->name('dashboard.')->group(function (){
   Route::get('/index','DashboardController@index')->name('index');
});
